package main

import (
	"fmt"
)

type person struct {
	firstName string
	lastName  string
}

func main() {
	// method 1
	//me := person{"Ilya","Sheidin"}
	// method 2
	// me := person{firstName: "Ilya", lastName: "Sheidin"}
	//method 3
	var me person
	me.firstName = "Ilya"
	me.lastName = "Sheidin"
	fmt.Printf("%+v", me)

}
